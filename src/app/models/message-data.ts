export class MessageData {

  private content: string;

  setContent(message: string): MessageData{

    this.content = message;
    return this;

  }

  getContent(): string{
    return this.content;
  }

}
