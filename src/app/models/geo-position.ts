export class GeoPosition {
  lat: string;
  lng: string;
}
