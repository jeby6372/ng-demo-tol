import { Component, OnInit } from '@angular/core';
import { mobiscroll, MbscCalendarOptions } from '@mobiscroll/angular';


mobiscroll.settings = {
  lang: 'fr',
  theme: 'ios',
  themeVariant: 'light'
};

const fromMonday = [],
  fromSaturday = [],
  twoWeeks = [];

for (let i; i < 7; i++) {
  fromMonday.push(new Date(2018, 0, 8 + i));
  fromSaturday.push(new Date(2018, 0, 6 + i));
}

for (let j; j < 14; j++) {
  twoWeeks.push(new Date(2018, 0, 8 + j));
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
