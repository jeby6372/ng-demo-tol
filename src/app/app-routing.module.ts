import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BootstrapComponent } from './components/bootstrap/bootstrap.component';


const routes: Routes = [
  { path: 'root', component: BootstrapComponent },
  { path: '', redirectTo: '/root', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }
