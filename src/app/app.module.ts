import { FormsModule } from '@angular/forms';
import { MbscModule } from '@mobiscroll/angular';
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BootstrapComponent } from "./components/bootstrap/bootstrap.component";
import { ListComponent } from "./panels/list/list.component";
import { DetailComponent } from "./panels/detail/detail.component";
import { IndexComponent } from "./panels/index/index.component";
import { HttpClientModule } from "@angular/common/http";
import { MainComponent } from "./views/main/main.component";

@NgModule({
  declarations: [
    AppComponent,
    BootstrapComponent,
    ListComponent,
    DetailComponent,
    IndexComponent,
    MainComponent
  ],
  imports: [ 
    FormsModule,  
    MbscModule, 
    BrowserModule,
    AppRoutingModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
