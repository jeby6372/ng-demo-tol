import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { UserDemo } from '../models/user-demo';

@Injectable({
  providedIn: 'root'
})

/**
 * NEWS GOOGLE API KEY : 8b8b70730d6f4334ba18132e61ce7f56
 */
export class DemoDataProviderService {
  private host: string = environment.hostDemo;
  private userUrl: string = environment.userDemoUrl;

  constructor(private http: HttpClient) {}

  getUsers(): Observable<UserDemo[]> {
    console.log(
      'DemoDataProviderService using url ' + this.host + this.userUrl
    );

    return this.http.get<UserDemo[]>(this.host + this.userUrl);

  }

  getUsersManaged(
    operationUid: string,
    filter: string,
    sortOrder: string,
    offset: number,
    limit: number
  ): Observable<UserDemo[]> {
    const headers = new HttpHeaders()
        .set('Accept-Encoding', 'gzip, deflate')
        .set('Accept-Charset', 'utf-8')
        // .set('Authorization', 'Basic xxxxxxx')
        // .set('Authorization', 'Bearer xxxxxxx')
        // .set('Cache-Control', 'no-cache')
        .set('From', 'emmanuel.brunet@open-si.org')
        .set('Max-Forwards', String(5));
      // .set('Proxy-Authorization', 'Basic xxxxxxxx')
      // .set('User-Agent', 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0')

    const params = new HttpParams()
      .set('operationUid', operationUid)
      .set('filter', filter)
      .set('sortOrder', sortOrder)
      .set('offset', offset.toString())
      .set('limit', limit.toString());

    console.log(
      'DemoDataProviderService using url ' + this.host + this.userUrl
    );

    return this.http.get<UserDemo[]>(this.host + this.userUrl, {
      headers,
      params
    });
  }


}
