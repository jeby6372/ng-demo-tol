import { TestBed } from '@angular/core/testing';

import { SignalizationService } from './signalization.service';

describe('SignalizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignalizationService = TestBed.get(SignalizationService);
    expect(service).toBeTruthy();
  });
});
