import { Injectable, NgModule } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransportService {

  constructor() { }
}

// Tree-shakable provider

@NgModule({
  providers: [TransportService],
})

export class TransportServiceModule {
}
