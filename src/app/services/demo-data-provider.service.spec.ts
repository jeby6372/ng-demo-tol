import { TestBed } from '@angular/core/testing';

import { DemoDataProviderService } from './demo-data-provider.service';

describe('DemoDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DemoDataProviderService = TestBed.get(DemoDataProviderService);
    expect(service).toBeTruthy();
  });
});
