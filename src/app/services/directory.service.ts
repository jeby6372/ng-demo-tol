import { Injectable, NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

/**
 * this.router.navigate(['/dest'])
 */
export class DirectoryService {

  constructor(private route: ActivatedRoute, private router: Router) { }
}

// Tree-shakable provider

@NgModule({
  providers: [DirectoryService],
})

export class DirectoryServiceModule {
}
