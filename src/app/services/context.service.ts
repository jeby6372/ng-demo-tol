import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ContextData } from '../models/context-data';

@Injectable({
  providedIn: 'root'
})
export class ContextService {

  private contextData: BehaviorSubject<ContextData> = new BehaviorSubject<ContextData>(new ContextData());
  state = this.contextData.asObservable();

  constructor() { }

  changeState(newState: ContextData){
    this.contextData.next(newState);
  }

  getState(): ContextData{
    return this.contextData.getValue();
  }

}
