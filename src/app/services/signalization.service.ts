import { Injectable, NgModule } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { MessageData } from '../models/message-data';

@Injectable({
  providedIn: 'root'
})
export class SignalizationService {

  private messageData: ReplaySubject<MessageData> = new ReplaySubject<MessageData>(5, 100); // ms
  message = this.messageData.asObservable();

  constructor() { }

  send(message: MessageData){
    this.messageData.next(message);
  }

}

// Tree-shakable provider

@NgModule({
  providers: [SignalizationService],
})

export class SignalizationServiceModule {
}
