import { Component, OnInit } from '@angular/core';
import { SignalizationService } from 'src/app/services/signalization.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  message: string;

  constructor(private sigService: SignalizationService) { }

  ngOnInit() {
    this.sigService.message.subscribe( msg =>{
      this.message = msg.getContent();
    });
  }

}
