import { Component, OnInit } from '@angular/core';
import { SignalizationService } from 'src/app/services/signalization.service';
import { DemoListDatasource } from 'src/app/datasources/demo-list-datasource';
import { DemoDataProviderService } from 'src/app/services/demo-data-provider.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  message: string;

  //  DemoList provides a BehaviourSubject based API as data input
  datasource: DemoListDatasource;

  displayedColumns = ['name', 'username', 'email'];

  constructor(private sigService: SignalizationService, private dataService: DemoDataProviderService) { }

  ngOnInit() {
    this.sigService.message.subscribe( msg =>{
      this.message = msg.getContent();
    });

    this.datasource = new DemoListDatasource(this.dataService);
    this.datasource.loadUsers();

  }

  rowClicked(row) {
    console.log('Row clicked: ', row);
}
}
