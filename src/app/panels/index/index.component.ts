import { Component, OnInit } from '@angular/core';
import { SignalizationService } from 'src/app/services/signalization.service';
import { MessageData } from 'src/app/models/message-data';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  indexList = [
    'item-1',
    'item-2',
    'item-3',
    'item-4',
    'item-5',
    'item-6',
  ];


  constructor(private sigService: SignalizationService) { }

  ngOnInit() {
  }

  click(message: string){
    const msg = new MessageData();
    this.sigService.send(msg.setContent(message));
  }

}
