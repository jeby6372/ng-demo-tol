import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { DemoDataProviderService } from '../services/demo-data-provider.service';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { UserDemo } from '../models/user-demo';

export class DemoListDatasource extends DataSource<UserDemo> {

private usersSubject = new BehaviorSubject<UserDemo[]>([]);
private loadingSubject = new BehaviorSubject<boolean>(false);
public loading$ = this.loadingSubject.asObservable();
// public loadingOnProgress = this.loadingSubject.asObservable();

constructor(private service: DemoDataProviderService) {
  super();
}

connect(collectionViewer: CollectionViewer): Observable<UserDemo[]> {
  return this.usersSubject.asObservable();
}

disconnect(collectionViewer: CollectionViewer): void {
  this.usersSubject.complete();
  this.loadingSubject.complete();
}

loadUsers() {

  this.loadingSubject.next(true);

  console.log('Loading users');

  this.service.getUsers().pipe(
    catchError(() => of([])), // TODO : use a message service to show a closable popup error
    finalize(() => this.loadingSubject.next(false))
  )
  .subscribe(users => this.usersSubject.next(users));

}


}
